
/*
    Task 1:

    Необходимо создать информацию о себе, используя переменные, в которых будет:
    -ваше имя,
    -ваш возраст,  
    -поле с вашим статусом о семейном положении, замужем/женаты (Либо истина , либо ложь)
    -по аналогии с предыдущим, поле с детьми

    Также необходимо определить тип данных всех ваших полей и вывести результат в консоль
*/

const name = 'Никита';
    age = 16;
let familyStatus = false;
    hasChildren = false;

console.log(typeof name);
console.log(typeof age);
console.log(typeof familyStatus);
console.log(typeof hasChildren);

/* 
    Task 2:

    Напишите скрипт, который находит площадь прямоугольника

    -высота 23см,
    -шириной 10см

    Каждая сущность должна находиться в своей переменной
*/
const height = 23;
     width = 10;
    result = height * width
    
    console.log(result);
    
/*
    Task 3:

    Напиши скрипт, который находит объем цилиндра
    
    -высота 10м  
    -площадь основания 4м

    Каждая сущность должна находиться в своей переменной
*/
const heightCylinder = 10;
     sBase = 4;
    result = heightCylinder * sBase
    
    console.log(result);

/*
    Task 4:

    Напиши рядом с каждым выражением , тот ответ который по вашему мнению выведет console.
    И потом сравните ваш результат с тем что на самом деле вывела консоль.
    
    Infinity - "1"  : Infinity
    "42" + 42   : 4242
    2 + "1 1"   : 21 1
    99 + 101    : 200
    "1" - "1"   : 0
    "Result: " + 10/2 : Result: 5
    3 + " bananas " + 2 + " apples " : 3 bananas 2 apples
*/
const denomination1 = Infinity - "1"; 
    denomination2 = "42" + 42;
    denomination3 = 2 + "1 1";
    denomination4 = 99 + 101;
    denomination5 = "1" - "1";
    denomination6 = "Result: " + 10/2;
    denomination7 = 3 + " bananas " + 2 + " apples ";

console.log(denomination1);
console.log(denomination2);
console.log(denomination3);
console.log(denomination4);
console.log(denomination5);
console.log(denomination6);
console.log(denomination7);

